﻿using System;

namespace StudentCG
{
    class Student
    {
        internal string Name;
        internal int Roll;
        internal int Batch;

        internal Student(string name, int roll, int batch)
        {
            this.Name = name;
            this.Roll = roll;
            this.Batch = batch;
        }

        internal static List<Student> GetStudentList(string Path)
        {
            string FileName = Path;
            string[] Lines = File.ReadAllLines(FileName);
            List<Student> StudentList = new List<Student>();

            foreach (string Line in Lines)
            {
                string[] StudentInfo = Line.Split(',');
                string Name = StudentInfo[0];
                int Roll = int.Parse(StudentInfo[1]);
                int Batch = int.Parse(StudentInfo[2]);

                StudentList.Add(new Student(Name, Roll, Batch));
            }

            return StudentList;
        }
    }
}