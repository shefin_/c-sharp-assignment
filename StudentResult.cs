﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentCG
{
    internal class StudentResult
    {
        internal int Roll;
        internal string Subject;
        internal double GP;

        internal StudentResult(int Roll, string Subject, double GP)
        {
            this.Roll = Roll;
            this.Subject = Subject;
            this.GP = GP;
        }

        internal static List<StudentResult> GetStudentResultList(string Path)
        {
            string FileName = Path;
            string[] Lines = File.ReadAllLines(FileName);
            List<StudentResult> StudentResultList = new List<StudentResult>();

            foreach (string Line in Lines)
            {
                string[] StudentInfo = Line.Split(',');
                int Roll = int.Parse(StudentInfo[0]);
                string Subject = StudentInfo[1];
                Double GP = Double.Parse(StudentInfo[2]);

                StudentResultList.Add(new StudentResult(Roll, Subject, GP));
            }

            return StudentResultList;
        }
    }
}
