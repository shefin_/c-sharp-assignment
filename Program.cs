﻿using System;
using System.IO;
using System.Numerics;

namespace StudentCG
{
    class Program
    {
        private static void Main(string[] args)
        {
            string StudentFilePath = "D:\\Projects\\C#\\StudentCG\\StudentCG\\StudentInfo.txt";
            string ResultFilePath = "D:\\Projects\\C#\\StudentCG\\StudentCG\\ResultInfo.txt";

            List<Student> StudentList = Student.GetStudentList(StudentFilePath);
            List<StudentResult> StudentResultList = StudentResult.GetStudentResultList(ResultFilePath);

            var StudentResultGroup = StudentList.GroupJoin(StudentResultList,
                                                            student => student.Roll,
                                                            result => result.Roll,
                                                            (student, results) => new
                                                            {
                                                                Name = student.Name,
                                                                Roll = student.Roll,
                                                                Results = results
                                                            });


            List <StudentCGInfo> StudentCGList = new List<StudentCGInfo>();

            foreach(var ResultInfo in StudentResultGroup)
            {
                string Name = ResultInfo.Name;
                int Roll = ResultInfo.Roll;

                double SummationGP = 0;
                foreach(var Result in ResultInfo.Results)
                {
                    SummationGP += Result.GP;
                }

                double CG = SummationGP / ResultInfo.Results.Count();

                StudentCGList.Add(new StudentCGInfo(Name, Roll, CG));
            }

            var SortedStudentCGList = StudentCGList.OrderBy(Info => Info.CG)
                                                .ThenBy(Info=>Info.Roll)
                                                .ThenBy(Info=>Info.Name);

            string OutputPath = "D:\\Projects\\C#\\StudentCG\\StudentCG\\output.txt";

            foreach(var StudentResultInfo in SortedStudentCGList)
            {
                String[] Line = { StudentResultInfo.Name + "," 
                                + StudentResultInfo.Roll.ToString() + "," 
                                + string.Format("{0:N2}", StudentResultInfo.CG) };

                File.AppendAllLines(OutputPath, Line);
            }
        }
    }
}