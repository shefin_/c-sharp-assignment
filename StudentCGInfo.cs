﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentCG
{
    internal class StudentCGInfo
    {
        internal string Name;
        internal int Roll;
        internal double CG;

        internal StudentCGInfo(string Name, int Roll, double CG)
        {
            this.Name = Name;
            this.Roll = Roll;
            this.CG = CG;
        }
    }
}
